<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'AwsSdk',
    ['path' => '/aws-sdk'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);


Router::prefix("admin",function($routes) {


    $routes->plugin("AwsSdk",["path"=>"/aws"],function($routes) {

        $routes->connect("/",[
            "controller"=>"BucketBrowser",
            "action"=>"index"
        ]);

        $routes->connect("/:controller",["action"=>"index"]);

        $routes->connect("/:controller/:action");

        $routes->fallbacks("DashedRoute");

    });

});
