<?php

function awsSdk($conf=[]) {
    $ops = array_merge([
		'region'=>'us-west-2',
		'version'=>'latest'
    ], $conf);
    return new \Aws\Sdk(
        $ops
    );
}

