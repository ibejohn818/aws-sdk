<?php
var_dump($objs);
?>
<?php $this->start("heading"); ?>
S3 Browser
<?php $this->end("heading"); ?>



<div id="admin-aws-bucket-browser">
    <div class="pagination-wrapper table-responsive">
        <table cellspacing='0' cellpadding='0' class="table table-striped">
            <thead>
                <?php if(isset($objs['Buckets'])): ?>
                <tr>
                    <th>Bucket</th>
                    <th>Date Created</th>
                </tr>
                <?php else: ?>
                <tr>
                    <th>
                       File Name 
                    </th>
                    <th></th>
                </tr>
                <?php endif; ?>
            </thead>
            <tbody>
                <?php if(isset($objs['Buckets'])): ?>
                    <?php 
                        foreach($objs['Buckets'] as $k=>$v):
                            $uri = $this->Url->build([
                                $v['Name']
                            ]);
                    ?>
                    <tr>
                        <td>
                            <a href='<?= $uri ?>'>
                                s3://<?= $v['Name'] ?>
                            </a>
                        </td>
                        <td>
                            <?= $v['CreationDate']->format("Y-m-d H:i:s") ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php 
                        foreach($objs['Contents'] as $k=>$v):
                            $uri = $this->Url->build([
                                $v['Name']
                            ]);
                    ?>
                    <tr>
                        <td>
                            <a href="">
                                <?php var_dump($v); ?>
                            </a>
                        </td>
                        <td></td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
