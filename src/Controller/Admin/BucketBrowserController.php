<?php

namespace AwsSdk\Controller\Admin;

use Cake\Event\Event;
use AwsSdk\Controller\AppController;


class BucketBrowserController extends AppController
{

    public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);

    }


    public function index($bucket=false, $prefix=false)
    {


        $sdk = awsSdk();

        $s3 = $sdk->createClient("s3");

        $s3->registerStreamWrapper();

        $conf = [];

        if($bucket) {
            $conf['Bucket'] = $bucket;
        }

        if($prefix) {
            $conf['Prefix'] = $prefix;
        } else {
            $conf['Delimeter'] = "/";
        }

        if(!isset($conf['Bucket'])) {
            $objs = $s3->listBuckets();
        } else {
            $objs = $s3->listObjectsV2($conf);
        }

        $this->set(compact(
            "objs",
            "bucket",
            "prefix"
        ));

    }

}
